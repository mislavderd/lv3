﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3RPOON
{
    class NotificationBuilder : IBuilder
    {
        private string author;
        private string title;
        private string text;
        private DateTime time;
        private ConsoleColor color;
        private Category level;

        public NotificationBuilder()
        {

        }

        public ConsoleNotification Build()
        {
            ConsoleNotification builder = new ConsoleNotification(author, title, text, time, level, color);
            return builder;
        }

        public IBuilder SetAuthor(string author)
        {
            this.author = author;
            return this;
        }

        public IBuilder SetColor(ConsoleColor color)
        {
            this.color = color;
            return this;
        }

        public IBuilder SetLevel(Category level)
        {
            this.level = level;
            return this;
        }

        public IBuilder SetText(string text)
        {
            this.text = text;
            return this;
        }

        public IBuilder SetTime(DateTime time)
        {
            this.time = time;
            return this;
        }

        public IBuilder SetTitle(string title)
        {
            this.title = title;
            return this;
        }
    }
}
