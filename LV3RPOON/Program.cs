﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3RPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            //  Logger logger = Logger.GetInstance();
            //logger.Log("Ivan Brkic");
            NotificationManager manager=new NotificationManager();
            NotificationBuilder builder = new NotificationBuilder();
            builder.SetAuthor("mislav")
                .SetColor(ConsoleColor.Yellow)
                .SetLevel(Category.INFO)
                .SetText("tekst proba")
                .SetTime(DateTime.Now)
                .SetTitle("LV3");
            ConsoleNotification console = builder.Build();
            manager.Display(console);

        }

    }
}
