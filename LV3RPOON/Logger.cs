﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3RPOON
{
    class Logger
    {
        private static Logger instance;
        private string filepath;
        private Logger()
        {
            filepath = @"C:\Users\derd\Desktop\OBJEKTNO\text.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void setPath(string filepath)
        {
            this.filepath = filepath;
        }
        public void Log(string message)
        {
            using(System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filepath))
            {
                writer.WriteLine(message);
            }
        }
    }
}
